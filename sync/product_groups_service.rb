class Sync::ProductGroupsService

  def initialize options = {}
    @options = options
  end

  def export
    type = @options[:type] || 'all'
    ws = Export.new @options

    method = type
    method = 'all' if type == 'by_date'
    method = 'qty' if @options[:qty]
    method = 'supplier' if @options[:supplier]

    ws.send(method)
  end

  def import
    type = @options[:type] || 'all'
    ws = Import.new @options

    method = type
    method = 'all' if type == 'by_date'
    method = 'qty' if @options[:qty]
    method = 'supplier' if @options[:supplier]

    ws.send(method)
  end


  # Import
  class Import < Sync::ImportBase
    attr_accessor :import_response_product_group

    def initialize params = {}
      @resource = 'products'
      @params = params
      @all = @params[:attrs] ? true : false
      @dates_range = dates_filter(params[:date_from],params[:date_to])
    end

    def all
      get_karimini_ids.map do |id|
        item karimini_id: id, all: @all
      end
    end

    def qty
      get_karimini_ids.map do |id|
        @product_group = ProductGroup.where(origin_id: id).first
        if @product_group.blank?
          next
        else
          service = Sync::ProductsService::Import.new(@product_group)
          service.item_qty @product_group.origin_id
        end
      end
    end

    def item opt = {}
      emporio_id = opt[:emporio_id] # import by emporio_id
      karimini_id = opt[:karimini_id] # import by karimini_id
      all = opt[:all] # import poduct group products
      qty = opt[:qty]

      if emporio_id
        @product_group = ProductGroup.find(emporio_id)
        karimini_id = @product_group.origin_id
      elsif karimini_id
        @product_group = ProductGroup.where(origin_id: karimini_id).first
      end

      if @product_group.blank?
        product_group_id = item_create karimini_id
        if product_group_id
          @product_group = ProductGroup.find(product_group_id)
        end
      else
        product_group_id = item_update @product_group.id
        if product_group_id
          @product_group = ProductGroup.find(product_group_id)
        end
      end

      if all
        with_products
        #item_specific_prices @product_group
      end

      item_categories @product_group
      item_images
    end

    def item_create id
      return if id == 0 || !id

      emporio_product_group_id = nil

      set_import_source "#{@resource}/%s" % id
      request_product = get_request

      if request_product
        if request_product.blank?
          Rails.logger.info("Can't get product [ID=%s]" % id)
          return
        end
        product_result = json_parse request_product, @resource.singularize
        return if product_result.nil?
        return if product_result.blank?

        @import_response_product_group = product_result

        product_group_data = build_data
        product_group_data[:origin_id] =  id

        product_group = ProductGroup.new(product_group_data)
        if product_group.save
          emporio_product_group_id = product_group.id
          Rails.logger.info('ProductGroup created [ID=%s]' % emporio_product_group_id)
        else
          Rails.logger.info(product_group.errors.full_messages.join('|'))
          return
        end
      end

      emporio_product_group_id
    end

    def item_update product_group_id
      product_group = ProductGroup.find(product_group_id)
      return unless product_group
      id = product_group.origin_id
      return if id == 0 || id == nil

      emporio_product_group_id = nil

      set_import_source "#{@resource}/%s" % id
      request_product = get_request

      if request_product
        if request_product.blank?
          Rails.logger.info("Can't get product [ID=%s]" % id)
          return
        end
        product_result = json_parse request_product, @resource.singularize
        return if product_result.nil?
        return if product_result.blank?

        @import_response_product_group = product_result

        product_group_data = build_data
        product_group_data[:origin_id] =  id

        emporio_product_group_id = product_group.id
        if product_group.update(product_group_data)
          Rails.logger.info('Product updated [ID=%s]' % emporio_product_group_id)
        else
          Rails.logger.info(product_group.errors.full_messages.join('|'))
          return
        end

      end

      emporio_product_group_id
    end

    def item_specific_prices product_group
      return unless product_group

      resource = "specific_prices"

      set_import_source "#{resource}", 'filter[id_product]=%s' % product_group.origin_id
      sprices = get_request
      return if sprices == '[]'
      sprices = json_parse sprices, resource
      unless sprices.blank?
        sprices = sprices.map{|i| i['id'].to_s.strip.to_i}
      end

      unless sprices.blank?
        sprices.each do |spid|
          set_import_source "#{resource}/%s" % spid
          specific_price = json_parse get_request, resource.singularize

          id_product_attribute = specific_price['id_product_attribute'].to_i
          data = {
              from: specific_price['from'],
              to: specific_price['to'],
              from_quantity: specific_price['from_quantity'],
              reduction: specific_price['reduction'],
              reduction_type: specific_price['reduction_type']
          }
          if id_product_attribute == 0
            data[:product_id] = 0
          else
            product = product_group.products.where(origin_comb_id: id_product_attribute)
            if product
              data[:product_id] = product.first.try(:id)
              puts data[:product_id]
              return if data[:product_id].nil?
            end
          end
          product_group.product_group_specific_prices.build(data).save!
        end
      end

    end

    def item_categories product_group
      resource = "categories"

      categories =  @import_response_product_group['associations'][resource]
      return if categories.blank?
      product_group_categories = []
      #product_group.product_group_categories.map(&:id)
      categories.map do |c|
        category = ProductGroupCategory.find_by_origin_id(c['id'])
        if category
          product_group_categories << category.id
        end
      end
      product_group.product_group_category_ids = product_group_categories
      product_group.product_group_categories
    end

    def item_images
      resource = "images"

      product_images = @import_response_product_group['associations'][resource]
      id_default_image = @import_response_product_group['id_default_image']
      if product_images
        @product_group.images.map(&:destroy)
        @product_group.product_group_images.map(&:destroy)
        product_images.each do |i|
          tmp = Tempfile.new('karimini', Dir.tmpdir, 'wb+')
          filename = tmp.path.gsub('/','-')[1..-1]

          set_import_source "#{resource}/#{@resource}/%s/%s" % [@product_group.origin_id,i['id'].to_i]
          begin
            request = open(@url)
          rescue => error
            Rails.logger.info('Error: %s' % error)
            next
          end

          if request
            IO.copy_stream(request, tmp)
            tmp.rewind
            new_file = "#{Rails.root}/public/uploads/tmp/#{filename}.jpg"
            FileUtils.mv(tmp.path, new_file)
            image = Image.new({
                                  image: File.open(new_file),
                                  origin_id: i['id']
                              })
            if image.save
              img = ProductGroupImage.new({
                                              product_group_id: @product_group.id,
                                              image_id: image.id,
                                              default: i['id'] == id_default_image ? true : false
                                          })
              img.save
              File.unlink(new_file)
            else
              Rails.logger.info(image.errors.full_messages.join('|'))
            end
          end

        end
      end
    end

    def with_products
      product_service = Sync::ProductsService::Import.new(@product_group)
      product_service.item @product_group.origin_id
    end


    private

    def build_data
      data = {
        name: @import_response_product_group['name'][0]['value'],
        description: @import_response_product_group['description'][0]['value'],
        product_type_id: ProductType.first.id,
        list_price: @import_response_product_group['wholesale_price'].to_f
      }
      data[:price] = build_price
      data[:active_state] = build_active_state
      data[:manufacturer_id] =  build_manufacturer unless build_manufacturer.nil?
      data[:karimini_data] = build_karimini_data

      %w(name description meta_description meta_keywords meta_title link_rewrite).each do |i|
        @import_response_product_group[i].each do |d|
          data[:karimini_data]["#{i}_#{set_languages[d['id'].to_i]}"] = d['value']
        end
      end

      data
    end

    def build_price
      tax_id = @import_response_product_group['id_tax_rules_group'].to_i
      #price = tax_id > 0 ? @import_response_product_group['price'].to_f/1.22 : @import_response_product_group['price'].to_f
      price = @import_response_product_group['price'].to_f
      price
    end

    def build_active_state
      @import_response_product_group['active'].to_i == 1 ? 'enable' : 'disable'
    end

    def build_manufacturer
      mid = nil
      manufacturer = set_manufacturer @import_response_product_group['id_manufacturer']
      mid = manufacturer.id if manufacturer
      mid
    end

    def set_manufacturer mid
      unless mid == '0' || mid.empty?
        manufacturer = Manufacturer.where(origin_id: mid).first
        if manufacturer.nil?
          ws = Sync::ManufacturersService::Import.new
          manufacturer = ws.item_create mid
        end
      end
      manufacturer
    end

    def build_karimini_data
      {
          date_add: @import_response_product_group['date_add'],
          date_upd: @import_response_product_group['date_upd'],
          tax_id: @import_response_product_group['id_tax_rules_group'],
          is_new: @import_response_product_group['new'],
          on_sale: @import_response_product_group['on_sale'],
          show_price: @import_response_product_group['show_price'],
          available_for_order: @import_response_product_group['available_for_order'],
          id_manufacturer: @import_response_product_group['id_manufacturer'],
          id_supplier: @import_response_product_group['id_supplier'],
          id_category_default: @import_response_product_group['id_category_default'],
          id_default_image: @import_response_product_group['id_default_image'],
          id_default_combination: @import_response_product_group['id_default_combination'],
          position_in_category: @import_response_product_group['position_in_category'],
          manufacturer_name: @import_response_product_group['manufacturer_name'],
          type: @import_response_product_group['type'],
          id_shop_default: @import_response_product_group['id_shop_default'],
          supplier_reference: @import_response_product_group['supplier_reference'],
          unit_price_ratio: @import_response_product_group['unit_price_ratio'],
          visibility: @import_response_product_group['visibility'],
          advanced_stock_management: @import_response_product_group['advanced_stock_management'],
          indexed: @import_response_product_group['indexed']
      }
    end


  end


  # Export
  class Export < Sync::ExportBase

      CONDITION_FOR_NEW = 30.days

      def initialize params = {}
        @resource = 'products'
        @params = params
        type = @params[:type] || 'all'
        @all = @params[:attrs] ? true : false
        @dates_range = (type == 'by_date') ? dates_filter(@params[:date_from], @params[:date_to]) : nil
      end

      def daily
        product_groups = ProductGroup.where(origin_id: nil).order('id asc')
        product_groups.each do |pg|
          item pg, {all: @all}
        end
      end

      def all
        product_groups = @dates_range ? ProductGroup.where(@dates_range) : ProductGroup.where(origin_id: nil)
        product_groups.order('origin_id desc').each do |pg|
          item pg, {all: @all}
        end
      end

      def qty
        product_groups = @dates_range ? ProductGroup.where(@dates_range) : ProductGroup.all
        product_groups.order('origin_id desc').each do |pg|
          if pg.blank?
            next
          else
            products = pg.products
            unless products.blank?
              products.each do |product|
                service = Sync::ProductsService::Export.new(pg)
                service.item_qty product
              end
            end
          end
        end
      end

      def supplier
        product_groups = @range ? ProductGroup.where(updated_at: @range) : ProductGroup.all
        product_groups.order('origin_id desc').each do |pg|
          if pg.blank?
            next
          else
            products = pg.products
            unless products.blank?
              products.each do |product|
                service = Sync::ProductsService::Export.new(pg)
                service.export_product_supplier product
              end
            end
          end
        end
      end

      def item product_group, opt = {}
        return unless product_group

        all = opt.try(:[], :all)

        @object = product_group
        data = build_data

        kid = product_group.origin_id
        action = set_action product_group
        data[:id] = kid if action == 'update'

        set_export_source @resource, action
        response = post_request data
        result = json_parse response
        return if result.nil?

        if result['status'] == 'success'
          product_group_id = result['product_id']['0'].to_i
          product_group.update!({origin_id: product_group_id}) if product_group_id > 0
        end

        images product_group

        if all
          all_products product_group
        end

        update_karimini_data product_group_id if product_group_id

        result
      end

      
def item_images pg
	images pg
end


      def update_karimini_data kid
        return unless kid

        product_group = ProductGroup.where(origin_id: kid).first
        return if product_group.nil?

        # Get product
        set_import_source 'products/%s' % kid
        karimini_product = get_request
        return if karimini_product.nil?

        data = karimini_data
        product = JSON.parse(karimini_product)['product']

        #data[:id_default_combination] = product_group.products.order('origin_comb_id asc').map(&:origin_comb_id).first || 0
        product_group.products.each do |i|
          @default_combination = (i.karimini_data[:default_on] == 1) ? i.origin_comb_id : 0
        end

        data[:id_default_combination] = @default_combination

        data[:date_add] = product['date_add']
        data[:date_upd] = product['date_upd']
        data[:tax_id] = product['id_tax_rules_group']
        data[:is_new] = product['new']
        data[:on_sale] = product['on_sale']
        data[:show_price] = product['show_price']
        data[:available_for_order] = product['available_for_order']
        data[:type] = product['type']
        data[:id_shop_default] = product['id_shop_default']
        data[:supplier_reference] = product['supplier_reference']
        data[:unit_price_ratio] = product['unit_price_ratio']
        data[:visibility] = product['visibility']
        data[:advanced_stock_management] = product['advanced_stock_management']
        data[:indexed] = product['indexed']

        product_group.update!({karimini_data: data})
      end


      def all_products product_group
        return unless product_group

        products = product_group.products
        unless products.blank?
          products.each do |product|
            service = Sync::ProductsService::Export.new(product_group)
            service.item product
          end
        end
      end



      def images product_group
        return unless product_group

        images = product_group.product_group_images
        return if images.blank?

        id_product = product_group.origin_id
        return unless sync?(id_product)

        images.each do |i|
          image = i.image
return if image.nil?
          kid = image.origin_id

          # Get image by id
          if sync?(kid)
            set_import_source 'images/products/%s/%s' % [id_product,kid]
            karimini_image = get_request

            action = 'create' if karimini_image.nil?
          end
          action = 'create' unless sync?(kid)

          if action == 'create'
            url = "#{WEBSERVICE_CONFIG['host']}webservice/products/images_index.php?action=create"
            data = {
                image_name: image.image.file.filename.to_s,
                image_url: "#{WEBSERVICE_CONFIG['emporio_host']}#{image.image.to_s}",
                product_id: id_product,
                cover: i.default ? 1 : 0,
                caption: product_group.name
            }
            Rails.logger.info('Export data %s' % data )
            Rails.logger.info('Fetching: %s' % url )
            begin
              request = RestClient.post url, data.to_json, :content_type => :json
            rescue RestClient::ExceptionWithResponse => error
              Rails.logger.info('Error: %s Url: %s' % [error,url])
              next
            end
#raise request.inspect
            if request
              result = JSON.parse(request)
              status = result.try(:[],'status')
              image_id = result.try(:[],'image_id')
              if status == 'success'
                image.update!({origin_id: image_id}) if image_id
              end
            end

          end


        end

      end


      private

      def build_data
        data = {
            name: @object.name,
            id_tax_rules_group: 1,
            price: @object.price,
            wholesale_price: @object.list_price,
            description: @object.description,
            active: @object.active_state == 'enable' ? 1 : 0,
        }
        data[:id_default_image] = build_default_image unless build_default_image.nil?
        if @object.manufacturer
          data[:id_manufacturer] = @object.manufacturer.origin_id unless @object.manufacturer.origin_id.nil?
        end

        data[:categories] = build_categories.join(',') unless build_categories.empty?
        data[:id_supplier] = build_supplier if build_supplier
        data[:id_category_default] = build_categories.last unless build_categories.empty?
        data[:id_default_combination] = build_default_combination if build_default_combination
        data[:on_sale] = build_on_sale
        data[:is_new] = build_is_new
        data[:date_add] = build_date karimini_data.try(:[],:date_add) || @object.created_at.utc.strftime("%Y-%m-%d %H:%M:%S")
        data[:date_upd] = build_date karimini_data.try(:[],:date_upd) || @object.updated_at.utc.strftime("%Y-%m-%d %H:%M:%S")
        data
      end


      def build_categories
        @object.product_group_categories.map(&:origin_id)
      end


      def build_default_combination
        default_combination = karimini_data.try(:[],:id_default_combination)

        return default_combination unless default_combination.nil? || default_combination == 0
      end

      def build_default_image
        img = @object.product_group_images
        return nil if img.blank?

        id = img.where(default: true).first.try(:image).try(:origin_id)
        id ? id : img.first.image.try(:origin_id)
      end

      def build_supplier
        @object.try(:products).try(:first).try(:supplier).try(:origin_id)
      end


      def build_on_sale
        @object.product_group_specific_prices.count > 0 ? 1 : 0
      end

      def build_is_new
        start_for_new = @object.created_at
        end_for_new = start_for_new + CONDITION_FOR_NEW
        now = Time.new

        (now > start_for_new && now < end_for_new) ? 1 : 0
      end

      def build_date date_in_string
        return nil if date_in_string.nil?

        date_add = date_in_string
        unless date_add.empty?
          date = Date._strptime(date_add,"%Y-%m-%d %H:%M:%S")
          unless date.nil?
            date_converted = Time.utc(date[:year], date[:mon], date[:mday], date[:hour], date[:min], date[:sec]).strftime("%Y-%m-%d %H:%M:%S")
          else
            date_converted = date_add
          end
        end
        date_converted
      end

      def karimini_data
        @object.karimini_data
      end

  end







end

