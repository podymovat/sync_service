class ApiShipmentsService

  def initialize opt = {}
    super()
    @options = opt
    @resource = 'shipments'
  end


  def import_shipment id, invoice
    # Get order info
    url = "#{WEBSERVICE_CONFIG['host']}api/orders/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % id
    Rails.logger.info('Fetching url: %s' % url)

    begin
      order_request = RestClient.get url
    rescue RestClient::ExceptionWithResponse => error
      Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
    end

    if order_request
      order_result = JSON.parse(order_request.body)
      return if order_result.blank?

      order_result = order_result.try(:[],'order')
      # Get order invoices
      url = "#{WEBSERVICE_CONFIG['host']}api/order_invoices/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}&filter[id_order]=%s" % order_result['id'].to_i
      Rails.logger.info('Fetching url: %s' % url)

      begin
        order_invoices_request = RestClient.get url
      rescue RestClient::ExceptionWithResponse => error
        Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
      end

      if order_invoices_request
        order_invoices_result = JSON.parse(order_invoices_request)
        return if order_invoices_result.blank?

        # Get order invoices info
        order_invoices_result.try(:[],'order_invoices').map do |oi|
          url = "#{WEBSERVICE_CONFIG['host']}api/order_invoices/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % oi['id'].to_i
          Rails.logger.info('Fetching url: %s' % url)

          begin
            order_invoice_request = RestClient.get url
          rescue RestClient::ExceptionWithResponse => error
            Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
          end


          if order_invoice_request
            order_invoice_result = JSON.parse(order_invoice_request)
            next if order_invoice_result.blank?

            order_invoice_result = order_invoice_result.try(:[],'order_invoice')
            data = {
                direction: 1,
                cost: order_invoice_result['total_shipping_tax_incl'],
                invoice: invoice,
                tracking_number: order_result['shipping_number'] || rand(10000),
                origin_id: order_invoice_result['id']
            }

            shipment = Shipment.new(data)
            if shipment.save
              Rails.logger.info('Shipment created [ID=%s]' % shipment.id)
            else
              Rails.logger.info(shipment.errors.full_messages.join('|'))
              next
            end

            # Get order details
            url = "#{WEBSERVICE_CONFIG['host']}api/order_details?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}&filter[id_order]=%s" % order_invoice_result['id_order'].to_i
            Rails.logger.info('Fetching url: %s' % url)

            begin
              order_details_request = RestClient.get url
            rescue RestClient::ExceptionWithResponse => error
              Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
            end


            if order_details_request
              order_details_result = JSON.parse(order_details_request)
              return if order_details_result.blank?

              order_details_result.try(:[],'order_details').map do |od|
                url = "#{WEBSERVICE_CONFIG['host']}api/order_details/%s?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % od['id'].to_i
                Rails.logger.info('Fetching url: %s' % url)

                begin
                  order_detail_request = RestClient.get url
                rescue RestClient::ExceptionWithResponse => error
                  Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
                end

                if order_detail_request
                  order_detail_result = JSON.parse(order_detail_request)
                  return if order_detail_result.blank?

                  order_detail_result = order_detail_result.try(:[],'order_detail')
                  product = Product.where(origin_comb_id: order_detail_result['product_attribute_id'])
                  next if product.blank?

                  shipment_product = ShipmentProduct.new({
                                                             shipment_id: shipment.id,
                                                             product_id:  product.id
                                                         })
                  if shipment_product.save
                    Rails.logger.info('ShipmentProduct created [ID=%s]' % shipment_product.id)
                  else
                    Rails.logger.info(shipment_product.errors.full_messages.join('|'))
                  end

                end

              end

            end



          end
        end



      end


    end







  end




end