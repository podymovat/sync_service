class Sync::ImportBase
  include Sync::ApiBase

  attr_accessor :result

  require 'tempfile'
  require 'open-uri'
  require 'fileutils'
  require 'nokogiri'


  def get_karimini_ids params = ''
    #params = @dates_range if @dates_range
    set_import_source @resource, params
    data = json_parse get_request, @resource
    ids = data.map{|i| i['id'].to_s.strip.to_i }
    ids.sort!
  end

  def dates_filter from = nil, to = nil
    now = Time.now.utc.strftime('%Y-%m-%d')
    now_day_earlier = (Time.now - 1.days).utc.strftime('%Y-%m-%d')
    range = nil

    from = from.to_datetime if from && from.kind_of?(String)
    from = from.utc.strftime('%Y-%m-%d') if from && from.is_a?(Date)
    to = to.to_datetime if to && to.kind_of?(String)
    to_day_earlier = (to - 1.days).utc.strftime('%Y-%m-%d') if to && to.is_a?(Date)
    to = to.utc.strftime('%Y-%m-%d') if to && to.is_a?(Date)

    range = [now_day_earlier,now] if from.nil? && to.nil?
    range = [to_day_earlier,to] if to && from.nil?
    range = [from,now] if from && to.nil? && from < now
    range = [now_day_earlier,now] if from && to.nil? && from == now
    range = nil if from && to.nil? && from > now
    range = [from,to] if from && to && from < to
    range = [to,from] if from && to && to < from

    range.nil? ? '' : "filter[date_upd]=[#{range.join(',')}]&date=1"
  end

  def set_action obj
    if in_sync? obj
      action = 'item_update'
    else
      action = 'item_create'
    end
  end

  def create_object obj
    if obj.save
      Rails.logger.info("#{@resource.singularize} created [ID=%s] %s" % [obj.id,obj])
      return obj
    else
      Rails.logger.info(obj.errors.full_messages.join('|'))
      return nil
    end
  end

  def update_object obj, data
    if obj.update(data)
      Rails.logger.info("#{@resource.singularize} updated [ID=%s] %s" % [obj.id,obj])
      return obj
    else
      Rails.logger.info(obj.errors.full_messages.join('|'))
      return nil
    end
  end

  def set_languages
    {1 => 'ru', 3 => 'en'}
  end

  def json_parse json, type
    return {} unless json

    begin
      result = JSON.parse(json).try(:[], type)
      result
    rescue
      nil
    end

  end

end
