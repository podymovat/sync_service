class Sync::ProductOptionsService

  def initialize options = {}
    @options = options
  end

  # Single export method
  def export
    ws = Export.new
    ws.all
  end

  # Single import method
  def import
    ws = Import.new
    ws.all
  end


  # Import class
  class Import < Sync::ImportBase

    def initialize options = {}
      @resource = 'product_options'
    end

    def all
      get_karimini_ids.map do |id|
        po = ProductOption.where(origin_id: id).first
        action = set_action po
        self.send(action, id)
        #item_image id if action == 'item_update'
      end
    end

    def item_create id
      return unless id

      set_import_source "#{@resource}/%s" %id
      @result = json_parse get_request, @resource.singularize
      data = build_data
      data[:origin_id] = id

      po = ProductOption.new(data)
      po = create_object po
      return nil if po.nil?

      product_option_values po

      po
    end


    def item_update id
      po = ProductOption.where(origin_id: id).first
      return if po.blank?
      return unless in_sync? po

      set_import_source "#{@resource}/%s" % id
      @result = json_parse get_request, @resource.singularize
      data = build_data
      data[:origin_id] = id

      po = update_object po, data
      return nil if po.nil?
      po.id
    end

    def product_option_values po
      @result['associations'].try(:[],'product_option_values').each do |p|
        id = p['id']
        next if id.nil?
        resource = 'product_option_values'
        set_import_source "#{resource}/%s" %id
        result = json_parse get_request, resource.singularize
        po.product_option_values.build({
                                           name: result['name'].last['value'],
                                           origin_id: result['id']
                                       }).save!

      end
    end


    private

    def build_data
      {
          name: @result['name'].last['value'],
      }
    end



  end


  # Export class
  class Export < Sync::ExportBase

    def initialize options = {}
      @resource = 'manufacturers'
    end


    def all
      manufaturers = Manufacturer.all.order('origin_id asc')
      manufaturers.each do |manufacturer|
        item manufacturer
      end
    end

    def item manufacturer
      return unless manufacturer

      @object = manufacturer
      data = build_data

      kid = manufacturer.origin_id
      action = set_action manufacturer
      data[:id] = kid if action == 'update'

      set_export_source @resource, action
      response = post_request data
      result = json_parse response

      if result['status'] == 'success'
        manufacturer_id = result['manufacturer_id']['0'].to_i
        manufacturer.update!({origin_id: manufacturer_id}) if manufacturer_id > 0
      end

      result
    end


    private

    def build_data
      {
          name: @object.name,
          description: @object.description,
          meta_title: @object.meta_title,
          meta_keywords: @object.meta_keywords,
          meta_description: @object.meta_description
      }
    end


  end







end
