class Sync::ProductsService

  def initialize product_group, options = {}
    @options = options
    @product_group = product_group
  end

  # Single export method
  def export options = {}
    ws = Export.new @product_group, params

    type = options.try(:[],:type) || 'all'
    ws.send(type)
  end

  # Single import method
  def import
    type = @options[:type] || 'all'
    params = build_import_params type
    ws = Import.new @product_group, params

    method = type
    method = 'all' if type == 'by_date'
    method = 'qty' if @options[:qty]

    ws.send(method)
  end


  private

  def build_import_params type
    date_from, date_to = nil,nil
    params = ''

    if type == 'by_date'
      date_from = @options[:date_from] ? @options[:date_from].strftime('%Y-%m-%d').to_s : Time.now.strftime('%Y-%m-%d').to_s
      date_to = @options[:date_to] ? @options[:date_to].strftime('%Y-%m-%d').to_s : Time.now.strftime('%Y-%m-%d').to_s
      #params = 'date=1&filter[date_upd]=[%s]%' % date_from
    end
    params
  end



  # Import
  class Import < Sync::ImportBase
    attr_accessor :import_response_product_group

    def initialize product_group, params = ''
      @product_group = product_group
      @resource = 'combinations'
      @params = params
    end


    def item id, opt = {}
      return unless id

      set_import_source 'combinations', 'filter[id_product]=%s' % id
      combinations = json_parse get_request, @resource
      return if combinations.nil?

      combination_ids = []
      unless combinations.blank?
        combination_ids = combinations.map{|i| i['id'].to_s.strip.to_i}
      end

      return if combination_ids.blank?
      combination_ids.each do |cid|
        product = Product.where(origin_comb_id: cid).first
        if product.blank?
          product_id = item_create id,cid
          if product_id
            product = Product.find product_id
            item_images product
            if update_sku(product)
              product.update!({approved: 1})
            end
          end
        else
          if product.in_sync?
            product_id = item_update product.id
            item_images product
          end
        end
      end
    end

    def item_create id, cid
      return if id == 0 || cid == 0
      Rails.logger.info("Start import: [ID=%s],[CID=%s]" % [id,cid])

      # Get product info
      Rails.logger.info('GET product info [ID=%s]' % id)
      url = "#{WEBSERVICE_CONFIG['host']}api/products/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % id.to_i
      url.gsub!(/(\s+)/,'')
      Rails.logger.info('Fetching url: %s' % url)
      begin
        request_product = RestClient.get url
      rescue RestClient::ExceptionWithResponse => error
        Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
        return
      end

      if request_product
        if request_product.blank?
          Rails.logger.info("Can't get product [ID=%s]" % id)
          return
        end
        product_result = JSON.parse(request_product)['product']
        return if product_result.blank?

        @import_response_product = product_result

        product_data = {
            manufacturer_number: product_result['supplier_reference'],
            transporter_incoming_price: 0,
            transporter_outgoing_price: 0,
            origin_category_id: product_result['id_category_default'],
            origin_default: cid == product_result['id_default_combination'].to_i ? true: false,
        }

        # Get supplier
        if supplier = get_supplier_id(product_result['id_supplier'], id)
          product_data[:supplier_id] = supplier  if supplier > 0
        end

        # Get combination by id
        Rails.logger.info('GET combination info [ID=%s]' % cid)
        url = "#{WEBSERVICE_CONFIG['host']}api/combinations/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % cid.to_i
        url.gsub!(/(\s+)/,'')
        Rails.logger.info('Fetching url: %s' % url)
        begin
          request_combination = RestClient.get url
        rescue RestClient::ExceptionWithResponse => error
          Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
          return
        end

        if request_combination
          combination = JSON.parse request_combination
          if combination.blank?
            Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
            return
          end

          if combination['combination']['associations']['images']
            @import_response_combination = combination['combination']['associations']['images'].map{|i| i['id'].to_i}
          end

          product_data[:weight] = combination['combination']['weight']
          product_data[:sku] = combination['combination']['reference']
          product_data[:origin_comb_id] = combination['combination']['id']

          product_data[:karimini_data] = {
              supplier_reference: combination['combination']['supplier_reference'],
              wholesale_price: combination['combination']['wholesale_price'],
              price: combination['combination']['price'],
              weight: combination['combination']['weight'],
              unit_price_impact: combination['combination']['unit_price_impact'],
              minimal_quantity: combination['combination']['minimal_quantity'],
              default_on: combination['combination']['default_on'],
              available_date: combination['combination']['available_date'],
              ean13: combination['combination']['ean13'],
              upc: combination['combination']['upc']
          }

          real_quantity = 0
          # Get quantity
          Rails.logger.info('GET stock availables by combination_id')
          url = "#{WEBSERVICE_CONFIG['host']}api/stock_availables/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}&filter[id_product_attribute]=%s" % cid.to_i
          url.gsub!(/(\s+)/,'')
          Rails.logger.info('Fetching url: %s' % url)
          begin
            request_stock_availables = RestClient.get url
          rescue RestClient::ExceptionWithResponse => error
            Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
            return
          end
          if request_stock_availables
            stock_availables = JSON.parse(request_stock_availables)['stock_availables'].map{|i| i['id'].to_i}
            s = stock_availables.max
            #stock_availables.each do |s|
              url = "#{WEBSERVICE_CONFIG['host']}api/stock_availables/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % s
              url.gsub!(/(\s+)/,'')
              Rails.logger.info('Fetching url: %s' % url)
              begin
                request_stock = RestClient.get url
              rescue RestClient::ExceptionWithResponse => error
                Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
                return
              end
              stock = JSON.parse(request_stock)['stock_available']

              product_data[:karimini_data][:stock_id] = s.to_i

              unless stock.blank?
                real_quantity = stock['quantity']
              end
            #end
          end
          # Get quantity
          product_data[:quantity] = real_quantity


          product = @product_group.products.build(product_data)
          if product.save
            emporio_product_id = product.id
            Rails.logger.info('Product created [ID=%s]' % emporio_product_id)
          else
            Rails.logger.info(product.errors.full_messages.join('|'))
            return
          end

          Rails.logger.info('Get product option values [ID=%s]' % id)
          combination_product_option_values = combination['combination']['associations'].try(:[],'product_option_values')
          if combination_product_option_values.blank?
            Rails.logger.info('Error: combination product option values is blank')
            return
          end
          combination_product_option_values.map do |j|
            # Get product option value by  id
            Rails.logger.info('GET product option value info [ID=%s]' % j['id'].to_i)
            url = "#{WEBSERVICE_CONFIG['host']}api/product_option_values/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % j['id'].to_i
            url.gsub!(/(\s+)/,'')
            Rails.logger.info('Fetching url: %s' % url)
            begin
              request_product_option_values = RestClient.get url
            rescue RestClient::ExceptionWithResponse => error
              Rails.logger.info('Error: %s Request: product_option_value[cid=%s]' % [error,j['id']])
              return
            end

            if request_product_option_values

              product_option_value = JSON.parse request_product_option_values
              if product_option_value.blank?
                Rails.logger.info('Error: product_option_value is blank')
                return
              end

              product_option_value_value =  product_option_value['product_option_value']['name'][0]['value']
              if product_option_value_value.blank?
                Rails.logger.info('Error: product_option_value_value is blank')
                return
              end

              product_option_id = product_option_value['product_option_value']['id_attribute_group'] || 0
              # Get product option by id
              Rails.logger.info('Get product options [product_option_id=%s]' % product_option_id.to_i)
              url = "#{WEBSERVICE_CONFIG['host']}api/product_options/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % product_option_id.to_i
              url.gsub!(/(\s+)/,'')
              Rails.logger.info('Fetching url: %s' % url)
              begin
                request_product_options = RestClient.get url
              rescue RestClient::ExceptionWithResponse => error
                Rails.logger.info('Error: %s Request: %s' % [error,url])
                return
              end

              if request_product_options

                product_option = JSON.parse request_product_options
                if product_option.blank?
                  Rails.logger.info('Error: product_option is blank')
                  return
                end

                product_option_name = product_option['product_option']['name'][0]['value']
                if product_option_name.blank?
                  Rails.logger.info('Error: product_option_name is blank')
                  return
                end

                # product option
                emporio_product_option = ProductOption.find_by_name(product_option_name)
                if emporio_product_option.blank?
                  emporio_product_option = ProductOption.new({name: product_option_name})
                  if emporio_product_option.save
                    Rails.logger.info('Product option created [ID=%s]' % emporio_product_option.id)
                  else
                    Rails.logger.info(emporio_product_option.errors.full_messages.join('|'))
                    return
                  end
                end

                # product option value
                emporio_product_option_value = ProductOptionValue.find_by_name(product_option_value_value)
                if emporio_product_option_value.blank?
                  emporio_product_option_value = ProductOptionValue.new({product_option_id: emporio_product_option.id, name: product_option_value_value})
                  if emporio_product_option_value.save
                    Rails.logger.info('Product option value created [ID=%s]' % emporio_product_option_value.id)
                  else
                    Rails.logger.info(emporio_product_option_value.errors.full_messages.join('|'))
                    return
                  end
                end
                product_option_combination = ProductOptionCombination.new({product_id: emporio_product_id,
                                                                           product_option_id: emporio_product_option.id,
                                                                           product_option_value_id: emporio_product_option_value.id,
                                                                           origin_id: j['id'].to_i})
                product_option_combination.save
              end
            end

          end

        end
      end
      product.id
    end

    def item_update product_id
      product = Product.find(product_id)
      return unless product

      id = @product_group.origin_id
      cid = product.origin_comb_id
      return if id == 0 || cid == 0

      # Get product info
      Rails.logger.info('GET product info [ID=%s]' % id)
      set_import_source 'products/%s' % id
      request_product = get_request

      if request_product

        product_result = json_parse(request_product, 'product')
        return if product_result.nil?
        return if product_result.blank?

        @import_response_product = product_result

        product_data = {
            origin_category_id: product_result['id_category_default'],
            origin_default: cid == product_result['id_default_combination'].to_i ? true: false,
        }

        # Get supplier
        if supplier = get_supplier_id(product_result['id_supplier'], id)
          product_data[:supplier_id] = supplier if supplier > 0
        end


        # Get combination by id
        set_import_source 'combinations/%s' % cid.to_i
        request_combinations = get_request

        if request_combinations

          combination = JSON.parse request_combinations
          return if combination.blank?

          if combination['combination']['associations']['images']
            @import_response_combination = combination['combination']['associations']['images'].map{|i| i['id'].to_i}
          end

          product_data[:weight] = combination['combination']['weight']
          product_data[:sku] = combination['combination']['reference']
          product_data[:origin_comb_id] = combination['combination']['id']

          product_data[:karimini_data] = {
              supplier_reference: combination['combination']['supplier_reference'],
              wholesale_price: combination['combination']['wholesale_price'],
              price: combination['combination']['price'],
              weight: combination['combination']['weight'],
              unit_price_impact: combination['combination']['unit_price_impact'],
              minimal_quantity: combination['combination']['minimal_quantity'],
              default_on: combination['combination']['default_on'],
              available_date: combination['combination']['available_date'],
              ean13: combination['combination']['ean13'],
              upc: combination['combination']['upc']
          }

          real_quantity = 0
          # Get quantity
          Rails.logger.info('GET stock availables by combination_id')
          url = "#{WEBSERVICE_CONFIG['host']}api/stock_availables/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}&filter[id_product_attribute]=%s" % cid.to_i
          url.gsub!(/(\s+)/,'')
          Rails.logger.info('Fetching url: %s' % url)
          begin
            request_stock_availables = RestClient.get url
          rescue RestClient::ExceptionWithResponse => error
            Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
            return
          end
          if request_stock_availables
            stock_availables = JSON.parse(request_stock_availables)['stock_availables'].map{|i| i['id'].to_i}
            s = stock_availables.max
            #stock_availables.each do |s|
              url = "#{WEBSERVICE_CONFIG['host']}api/stock_availables/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % s
              url.gsub!(/(\s+)/,'')
              Rails.logger.info('Fetching url: %s' % url)
              begin
                request_stock = RestClient.get url
              rescue RestClient::ExceptionWithResponse => error
                Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
                return
              end
              stock = JSON.parse(request_stock)['stock_available']

              product_data[:karimini_data][:stock_id] = s.to_i

              unless stock.blank?
                real_quantity = stock['quantity']
              end
            #end
          end
          # Get quantity
          product_data[:quantity] = real_quantity

          # Update product
          emporio_product_id = product.id
          if product.update(product_data)
            Rails.logger.info('Emporio response: Product updated [ID=%s]' % emporio_product_id)
          else
            Rails.logger.info(product.errors.full_messages.join('|'))
            return
          end

          # Set emporio product option combinations
          # Delete emporio product option combinations
          product.product_option_combinations.map(&:destroy)

          combination_product_option_values = combination['combination']['associations'].try(:[],'product_option_values')
          return if combination_product_option_values.blank?
          combination_product_option_values.map do |j|
            # Get product option value by  id
            url = "#{WEBSERVICE_CONFIG['host']}api/product_option_values/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % j['id'].to_i
            Rails.logger.info('GET product option value [ID=%s]' % j['id'])
            url.gsub!(/(\s+)/,'')
            Rails.logger.info('Fetching url: %s' % url)
            begin
              request_product_option_values = RestClient.get url
            rescue RestClient::ExceptionWithResponse => error
              Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
            end

            if request_product_option_values

              product_option_value = JSON.parse request_product_option_values
              return if product_option_value.blank?

              product_option_value_value =  product_option_value['product_option_value']['name'][0]['value']
              return if product_option_value_value.blank?

              product_option_id = product_option_value['product_option_value']['id_attribute_group'] || 0
              # Get product option by id
              url = "#{WEBSERVICE_CONFIG['host']}api/product_options/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % product_option_id.to_i
              Rails.logger.info('GET product options [ID=%s]' % product_option_id.to_i)
              url.gsub!(/(\s+)/,'')
              Rails.logger.info('Fetching url: %s' % url)
              begin
                request_product_options = RestClient.get url
              rescue RestClient::ExceptionWithResponse => error
                Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
              end

              if request_product_options
                product_option = JSON.parse request_product_options
                return if product_option.blank?

                product_option_name = product_option['product_option']['name'][0]['value']
                return if product_option_name.blank?

                # product option
                emporio_product_option = ProductOption.find_by_name(product_option_name)
                if emporio_product_option.blank?
                  emporio_product_option = ProductOption.new({name: product_option_name})
                  if emporio_product_option.save
                    Rails.logger.info('Product option created [ID=%s]' % emporio_product_option.id)
                  else
                    Rails.logger.info(emporio_product_option.errors.full_messages.join('|'))
                    return
                  end
                end

                # product option value
                emporio_product_option_value = ProductOptionValue.find_by_name(product_option_value_value)
                if emporio_product_option_value.blank?
                  emporio_product_option_value = ProductOptionValue.new({product_option_id: emporio_product_option.id, name: product_option_value_value})
                  if emporio_product_option_value.save
                    Rails.logger.info('Product option value created [ID=%s]' % emporio_product_option_value.id)
                  else
                    Rails.logger.info(emporio_product_option_value.errors.full_messages.join('|'))
                    return
                  end
                end
                product_option_combination = ProductOptionCombination.new({product_id: emporio_product_id,
                                                                           product_option_id: emporio_product_option.id,
                                                                           product_option_value_id: emporio_product_option_value.id})
                product_option_combination.save
              end

            end
          end
          item_images product
        end

      end
      product.id
    end

    def item_qty id
      return unless id

      set_import_source 'combinations', 'filter[id_product]=%s' % id
      combinations = json_parse get_request, @resource
      return if combinations.nil?

      combination_ids = []
      unless combinations.blank?
        combination_ids = combinations.map{|i| i['id'].to_s.strip.to_i}
      end

      return if combination_ids.blank?
      combination_ids.each do |cid|
        product = Product.where(origin_comb_id: cid).first
        unless product.blank?
          if product.in_sync?
            return unless product
            id = @product_group.origin_id
            cid = product.origin_comb_id
            return if id == 0 || cid == 0

            Rails.logger.info('GET product info [ID=%s]' % id)
            set_import_source 'products/%s' % id
            request_product = get_request

            if request_product

              product_data = {}
              product_data[:karimini_data] = {}
              set_import_source 'combinations/%s' % cid.to_i
              request_combinations = get_request

              if request_combinations

                combination = JSON.parse request_combinations
                return if combination.blank?

                real_quantity = 0
                Rails.logger.info('GET stock availables by combination_id')
                url = "#{WEBSERVICE_CONFIG['host']}api/stock_availables/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}&filter[id_product_attribute]=%s" % cid.to_i
                url.gsub!(/(\s+)/,'')
                Rails.logger.info('Fetching url: %s' % url)
                begin
                  request_stock_availables = RestClient.get url
                rescue RestClient::ExceptionWithResponse => error
                  Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
                  return
                end
                if request_stock_availables
                  stock_availables = JSON.parse(request_stock_availables)['stock_availables'].map{|i| i['id'].to_i}
                  s = stock_availables.max
#                  stock_availables.each do |s|
                    url = "#{WEBSERVICE_CONFIG['host']}api/stock_availables/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % s
                    url.gsub!(/(\s+)/,'')
                    Rails.logger.info('Fetching url: %s' % url)
                    begin
                      request_stock = RestClient.get url
                    rescue RestClient::ExceptionWithResponse => error
                      Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
                      return
                    end
                    stock = JSON.parse(request_stock)['stock_available']
                    product_data[:karimini_data][:stock_id] = s.to_i

                    unless stock.blank?
                      real_quantity = stock['quantity']
                    end
#                  end
                end
                product_data[:quantity] = real_quantity

                emporio_product_id = product.id
                if product.update(product_data)
                  Rails.logger.info('Product updated [ID=%s]' % emporio_product_id)
                else
                  Rails.logger.info(product.errors.full_messages.join('|'))
                  return
                end

              end

            end




          end
        end
      end

    end

    def item_images product
      product_images = @import_response_product['associations']['images']
      combination_images = @import_response_combination || []
      if product_images
        product.images.map(&:destroy)
        combination_images.each do |i|
          tmp = Tempfile.new('karimini', Dir.tmpdir, 'wb+')
          filename = tmp.path.gsub('/','-')[1..-1]
          url = "#{WEBSERVICE_CONFIG['host']}api/images/products/%s/%s?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % [@product_group.origin_id,i.to_i]
          Rails.logger.info('Fetching url: %s' % url)
          begin
            request = open(url)
          rescue => error
            Rails.logger.info('Error: %s' % error)
            next
          end

          if request
            IO.copy_stream(request, tmp)
            tmp.rewind
            new_file = "#{Rails.root}/public/uploads/tmp/#{filename}.jpg"
            FileUtils.mv(tmp.path, new_file)
            image = product.images.build({
                                             image: File.open(new_file),
                                             origin_id: i.to_i,
                                         })
            if image.save
              img = ProductImage.new({
                                         product_id: product.id,
                                         image_id: image.id
                                     })
              img.save
              File.unlink(new_file)
            else
              Rails.logger.info(image.errors.full_messages.join('|'))
            end
          end

        end
      end
    end

    def update_sku product
      return if product.blank?

      return product.generate_sku! ? true : false
    end

    def get_supplier_id sid, id_product
      id = 0
      id_supplier = sid.to_i #unless sid.kind_of?(Integer)
      #return 0 unless sid.kind_of?(Integer) || sid > 0

      if id_supplier == 0
        set_import_source 'product_suppliers', 'filter[id_product]=%s' % id_product
        responce = get_request
        product_supplier_id = json_parse(responce, 'product_suppliers')
        return 0 if product_supplier_id.nil?
        product_supplier_id = product_supplier_id.first['id'].to_i

        set_import_source 'product_suppliers/%s' % product_supplier_id
        product_supplier = json_parse(get_request, 'product_supplier')
        id_supplier = product_supplier['id_supplier'].to_i
      end
      return 0 if id_supplier == 0

      supplier = Supplier.find_by_origin_id(id_supplier)
      if supplier.blank?
        ws_supplier = ApiSuppliersService.new
        if id = ws_supplier.import_create(id_supplier) && id.kind_of?(Integer)
          supplier = Supplier.find_by_origin_id(id_supplier)
        end
      end
      id = supplier.id
      id
    end

    private

    def build_data
      data = {
          name: @import_response_product_group['name'][0]['value'],
          description: @import_response_product_group['description'][0]['value'],
          product_type_id: ProductType.first.id,
          list_price: @import_response_product_group['wholesale_price'].to_f
      }
      data[:price] = build_price
      data[:active_state] = build_active_state
      data[:manufacturer_id] =  build_manufacturer unless build_manufacturer.nil?
      data[:karimini_data] = build_karimini_data

      %w(name description meta_description meta_keywords meta_title link_rewrite).each do |i|
        @import_response_product_group[i].each do |d|
          data[:karimini_data]["#{i}_#{set_languages[d['id'].to_i]}"] = d['value']
        end
      end

      data
    end



  end


  # Export
  class Export < Sync::ExportBase

    CONDITION_FOR_NEW = 30.days

    def initialize product_group, params = {}
      @product_group = product_group
      @resource = 'combinations'
      @params = params
    end

    def item product
      return unless product

      @object = product
      data = build_data

      kid = product.origin_comb_id
      if kid.nil? || kid == 0
        action = 'create'
      else
        set_import_source 'combinations/%s' % kid
        karimini_combination = get_request

        if karimini_combination.nil?
          action = 'create'
        else
          action = 'update'
          data['id'] = kid
        end
      end

      set_export_source @resource, action
      response = post_request data
      #raise response.inspect
      result = json_parse response
      return if result.nil?

      if result['status'] == 'success'
        product_id = result['combination_id'].to_i
        combination = result['combination']
        combination.each do |c|
          product_option_id = c['product_option_id'].to_i
          emporio_product_option_id = c['emporio_product_option_id'].to_i
          product_option_value_id = c['product_option_value_id'].to_i
          product_option_combination_id = c['product_option_combination_id'].to_i

          if product_option_value_id > 0 && product_option_combination_id > 0
            product_option_combination = ProductOptionCombination.find(product_option_combination_id)
            if product_option_combination
              product_option_combination.update!({origin_id: product_option_value_id})
            end
          end

          if product_option_id > 0 && emporio_product_option_id > 0
            product_option = ProductOption.find(emporio_product_option_id)
            if product_option
              product_option.update!({origin_id: product_option_id})
            end
          end
        end

        if product_id > 0
          product.update!({origin_comb_id: product_id})
          export_stock product
          export_product_supplier product
        end

        # update karimini_data
        update_karimini_data product_id if product_id && product_id > 0
      end
    end


    def item_qty product
      return unless product

      export_stock product
    end


    def update_karimini_data kid
      return unless kid

      # Get product
      set_import_source 'combinations/%s' % kid
      karimini_combination = get_request
      return if karimini_combination.nil?

      data = karimini_data
      combination = JSON.parse(karimini_combination)

      data[:default_on] = combination['combination']['default_on']
      data[:supplier_reference] = combination['combination']['supplier_reference'],
          data[:wholesale_price] = combination['combination']['wholesale_price']
      data[:price] = combination['combination']['price']
      data[:weight] = combination['combination']['weight']
      data[:unit_price_impact] = combination['combination']['unit_price_impact']
      data[:minimal_quantity] = combination['combination']['minimal_quantity']
      data[:available_date] = combination['combination']['available_date']
      data[:ean13] = combination['combination']['ean13']
      data[:upc] = combination['combination']['upc']

      product = @product_group.products.where(origin_comb_id: kid).first
      product.update!({karimini_data: data})
    end


    def export_stock product
      return unless product

      kid = product.origin_comb_id

      set_import_source 'stock_availables', 'filter[id_product_attribute]=%s' % kid
      request = get_request
      return if request == '[]'
      stock_id = JSON.parse(request).try(:[],'stock_availables').try(:[],0).try(:[],'id')

      unless stock_id.nil?
        url = "#{WEBSERVICE_CONFIG['host']}webservice/products/index.php?action=stock_update"
        data = {
            id: stock_id,
            id_product: @product_group.origin_id,
            id_product_attribute: kid,
            quantity: product.quantity
        }
        request = RestClient.post url, data.to_json, :content_type => :json
      end
    end


    def export_product_supplier product
      return unless product
      #return if product.supplier_id.nil?
      return if product.origin_comb_id == 0 || product.origin_comb_id.nil?

      kid = @product_group.origin_id
      cid = product.origin_comb_id
      sid = product.try(:supplier).try(:origin_id)
      sid = product.product_group.try(:supplier).try(:origin_id) if sid.nil?
      return if sid.nil?

      @object = product
      data = {
          id_product: kid,
          id_product_attribute: cid,
          id_supplier: sid,
          product_supplier_reference: product.manufacturer_number.strip
      }

      set_import_source 'product_suppliers', 'filter[id_product_attribute]=%s&filter[id_supplier]=%s' % [cid,sid]
      responce = get_request
      if responce.nil?
        action = 'create'
      else
        action = 'update'
        if responce == '[]'
          action = 'create'
        else
          ps_id = json_parse(responce).try(:[], 'product_suppliers').try(:first).try(:[],'id')
          unless ps_id.nil?
            data[:id] = ps_id unless ps_id.nil?
          end
        end

      end
      set_export_source 'product_suppliers', action
      request = post_request data
      result = json_parse request
      return if result.nil?

      if result['status'] == 'success'
        product_supplier_id = result['product_supplier_id'].to_i
        if product_supplier_id > 0
          karimini_data[:product_supplier_id] = product_supplier_id
          product.update!({karimini_data: karimini_data})
        end

      end

    end



    private

    def build_data
      data = {
          id_product: @product_group.origin_id,
          #quantity:  @object.quantity,
          minimal_quantity: 1,
          #weight: @object.weight,
          reference: @object.sku,
          default_on: build_default_combination,
          images: @object.images.blank? ? nil : build_combination_images
      }
      combinations = []
      @object.product_option_combinations.map do |i|
        c = {
            product_option_combination_id: i.id,
            attribute_id: i.product_option.origin_id,
            emporio_product_option_id: i.product_option.id,
            attribute_name: i.product_option.name,
            attribute_value: i.product_option_value.name.strip
        }
        c[:id] = i.origin_id unless i.origin_id.nil? || i.origin_id == 0
        combinations << c
      end
      data[:product_attributes] = combinations

      data
    end


    def build_combination_images
      images = []
      @object.images.map do |i|
        images << {
            id_product: @product_group.origin_id,
            id_combination: @object.origin_comb_id,
            image_id: i.origin_id,
        }

      end
      images
    end


    def build_default_combination
      @default_on = karimini_data.try(:[], :default_on)

      if @default_on.nil?
        @default_on = 0
        pid = @product_group.products.map(&:id).first
        if @object.id == pid
          @default_on = 1
        end
      end
      @default_on
    end



    def karimini_data
      @object.karimini_data
    end


    def build_product_combinations
      combinations = []
      @object.product_option_combinations.map do |i|
        combinations << {
            id_product: @product_group.origin_id,
            quantity:  @object.quantity,
            minimal_quantity: 1,
            weight: @object.weight,
            reference: @object.sku,
            attribute_name: i.product_option.name,
            attribute_value: i.product_option_value.name,
            default_combination: @object.origin_default,
            images: @object.images.blank? ? nil : build_combination_images
        }

      end
      combinations
    end


  end







end
