module Sync::ApiBase
  extend self

  def host
    @host = WEBSERVICE_CONFIG['host']
  end

  def ws_key
    @ws_key = WEBSERVICE_CONFIG['key']
  end

  def format
    @format = WEBSERVICE_CONFIG['format']
  end

  def emporio_host
    @emporio_host = WEBSERVICE_CONFIG['emporio_host']
  end

  def get_request
    Rails.logger.info('Fetching url: %s' % @url)
    begin
      request = RestClient.get @url
    rescue RestClient::ExceptionWithResponse => error
      Rails.logger.info('Error: %s Url: %s' % [error,@url])
    end
    request
  end

  def post_request data
    Rails.logger.info('Export data: %s' % data)
    Rails.logger.info('Fetching: %s' % @url )
    begin
      request = RestClient.post @url, data.to_json, :content_type => :json
    rescue RestClient::ExceptionWithResponse => error
      Rails.logger.info('Error: %s Url: %s' % [error,@url])
    end
    request
  end

  def import_url params = nil
    params = params.nil? ? '' : "&#{params}"
    @url = "#{@host}api/#{@source}?ws_key=#{@ws_key}&output_format=#{@format}#{params}"
    clean_url
  end

  def export_url action
    @url = "#{@host}webservice/#{@source}/index.php?action=#{action}"
    clean_url
  end

  def clean_url
    @url.gsub!(/(\s+)/,'')
  end

  def set_import_source v, params = nil
    host; ws_key; format
    @source = v
    import_url params
  end

  def set_export_source v, action
    host; emporio_host
    @source = v
    export_url action
  end

  def sync? v
    v.nil? || v == 0 ? false : true
  end

  def in_sync? obj
    kid = obj.try(:origin_id)
    return false unless kid
    return false if kid.nil? || kid == 0
    true
  end

  def import_test e = nil
    entity = e || 'products'
    set_import_source entity
    puts "Try to send GET request..."
    response = get_request
    puts "Server response: #{response.code}" if response

    {
        'HOST' => @host,
        'KEY' => @ws_key,
        'FORMAT' => @format,
        'BUILD URL' => @url
    }
  end

  def export_test e = nil, a = nil
    entity = e || 'products'
    action = a || 'create'
    set_export_source entity, action
    {
        'HOST' => @host,
        'EMPORIO HOST' => @emporio_host,
        'BUILD URL' => @url
    }
  end

end
