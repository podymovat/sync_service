class Sync::SuppliersService

  def initialize options = {}
    @options = options
  end

  def export
    ws = Export.new
    ws.all
  end

  def import
    ws = Import.new
    ws.all
  end


  # Import class
  class Import < Sync::ImportBase

    def initialize options = {}
      @resource = 'suppliers'
    end

    def all
      get_karimini_ids.map do |id|
        supplier = Supplier.where(origin_id: id).first
        action = set_action supplier
        self.send(action, id)
        #item_image id if action == 'item_update'
      end
    end

    def item_create id
      return unless id

      # Get supplier info
      Rails.logger.info('GET supplier info [ID=%s]' % id)
      url = "#{WEBSERVICE_CONFIG['host']}api/suppliers/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % id.to_i
      url.gsub!(/(\s+)/,'')
      Rails.logger.info('Fetching url: %s' % url)
      begin
        request = RestClient.get url
      rescue RestClient::ExceptionWithResponse => error
        Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
        return
      end
      result = JSON.parse(request.body)['supplier']
      data = {
          name: result['name'],
          description: request['description'][0]['value'],
          origin_id: id
      }

      supplier = Supplier.new(data)
      if supplier.save
        Rails.logger.info('Product created [ID=%s] %s' % [supplier.id,supplier])
      else
        Rails.logger.info(supplier.errors.full_messages.join('|'))
      end
      supplier.id
    end


    def item_update id
      supplier = Supplier.find(id)
      return if supplier.blank?
      return unless supplier.in_sync?
      id = supplier.origin_id
      # Get supplier info
      Rails.logger.info('GET supplier info [ID=%s]' % id)
      url = "#{WEBSERVICE_CONFIG['host']}api/suppliers/%s/?ws_key=#{WEBSERVICE_CONFIG['key']}&output_format=#{WEBSERVICE_CONFIG['format']}" % id.to_i
      url.gsub!(/(\s+)/,'')
      Rails.logger.info('Fetching url: %s' % url)
      begin
        request = RestClient.get url
      rescue RestClient::ExceptionWithResponse => error
        Rails.logger.info('Error[ID=%s]: %s Request: %s' % [id,error,url])
        return
      end
      result = JSON.parse(request.body)['supplier']
      data = {
          name: result['name'],
          description: request['description'][0]['value'],
          origin_id: id
      }
      if supplier.update(data)
        Rails.logger.info('Product updated [ID=%s] %s' % [supplier.id,supplier])
      else
        Rails.logger.info(supplier.errors.full_messages.join('|'))
      end
      supplier.id
    end

    private

    def build_data
      {
          name: @result['name'],
          description: @result['description'][0]['value'],
          short_description: @result['short_description'][0]['value'],
          meta_title: @result['meta_title'][0]['value'],
          meta_keywords: @result['meta_keywords'][0]['value'],
          meta_description: @result['meta_description'][0]['value']
      }
    end



  end



end
