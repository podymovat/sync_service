class Sync::CategoriesService

  def initialize options = {}
    @options = options
  end

  # Single export method
  def export
    ws = Export.new
    ws.all
  end

  # Single import method
  def import
    ws = Import.new
    ws.all
  end


  # Import class
  class Import < Sync::ImportBase

    def initialize options = {}
      @resource = 'categories'
    end

    def get_root_ids
      set_import_source @resource, 'filter[is_root_category]=1'
      categories = get_request
      return if categories.nil?

      categories = json_parse get_request, @resource
      ids = categories.map{|i| i['id'].to_s.strip.to_i}
      ids
    end

    def all
      root_ids = get_root_ids
      return 'No root categories' if root_ids.blank?
      root_ids.each do |root|
        item root
      end
    end

    def item id, parent_id = nil
      return unless id

      set_import_source "#{@resource}/%s" % id
      @result = json_parse get_request, @resource.singularize

      # Build data
      data = build_data

      if parent_id
        data[:parent_id] = parent_id
      end

      category = ProductGroupCategory.where(origin_id: id).first
      if category.blank?
        category = ProductGroupCategory.new(data)
        if category.save
          Rails.logger.info("Category created ID[#{category.id}]")
        else
          Rails.logger.info(category.errors.full_messages.join(';'))
          return
        end
        item_image id
      else
        if category.update!(data)
          Rails.logger.info("Category updated ID[#{category.id}]")
        else
          Rails.logger.info(category.errors.full_messages.join(';'))
        end
      end


      # get subcategories
      set_import_source @resource, 'filter[id_parent]=%s' % id
      subcategories = get_request
      unless subcategories == '[]'
        subcategories = json_parse subcategories, @resource
        unless subcategories.blank?
          ids = subcategories.map{|i| i['id'].to_s.strip.to_i}
          ids.each do |id|
            item id, category.id
          end
        end
      end
      #category
    end


    def item_image id
      return unless id

      tmp = Tempfile.new('karimini-category-image', Dir.tmpdir, 'wb+')
      filename = tmp.path.gsub('/','-')[1..-1]

      set_import_source "images/#{@resource}/%s" % id
      begin
        request = open(@url)
      rescue => error
        Rails.logger.info('Error: %s' % error)
      end

      if request
        IO.copy_stream(request, tmp)
        tmp.rewind
        new_file = "#{Rails.root}/public/uploads/tmp/#{filename}.jpg"
        FileUtils.mv(tmp.path, new_file)
        data = {
            image: File.open(new_file),
        }

        category = ProductGroupCategory.where(origin_id: id)
        if category.update(data)
          File.unlink(new_file)
        else
          Rails.logger.info(image.errors.full_messages.join('|'))
        end

      end
    end


    private

    def build_data
      {
        name: @result['name'][0]['value'],
        description: @result['description'][0]['value'],
        meta_title: @result['meta_title'][0]['value'],
        meta_keywords: @result['meta_keywords'][0]['value'],
        meta_description: @result['meta_description'][0]['value'],
        origin_id: @result['id']
      }
    end



  end


  # Export class
  class Export < Sync::ExportBase

    def initialize options = {}
      @resource = 'categories'
    end


    def all
      categories = ProductGroupCategory.all.order('origin_id asc')
      categories.each do |category|
        item category
      end
    end

    def item category
      return unless category

      @object = category
      data = build_data

      kid = category.origin_id
      action = set_action category
      data[:id] = kid if action == 'update'

      set_export_source @resource, action
      response = post_request data
      result = json_parse response

      if result['status'] == 'success'
        category_id = result['category_id']['0'].to_i
        category.update!({origin_id: category_id}) if category_id > 0
      end

      result
    end


    private

    def build_data
      data = {
              name: @object.name,
              description: @object.description,
              meta_title: @object.meta_title,
              meta_keywords: @object.meta_keywords,
              meta_description: @object.meta_description
      }
      data[:is_root_category] = build_is_root
      data[:id_parent] = build_id_parent  unless @object.parent_id.nil?
      data
    end

    def build_is_root
      @object.ancestry.nil? ? 1 : 0
    end

    def build_id_parent
      parent = @object.parent_id
      category = ProductGroupCategory.find(parent)
      kid = category.origin_id

      set_import_source "#{@resource}/%s" % kid
      #result = json_parse get_request
    end


  end







end
