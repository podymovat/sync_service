class Sync::UlissProductsService

  def initialize options = {}
    @options = options
  end

  def import
    type = @options[:type] || 'all'
    ws = Import.new @options

    ws.send(type)
  end


  # Import
  class Import < Sync::ImportBase

    def initialize params = {}
      @resource = 'products'
      @host = 'http://www.glamourstore.it/'
      @ws_key = '9UAMMJYRGTNGIMVYQ8TAL94SNMVF88CC'
      @name = 'Uliss'
    end

    def get_ids
      set_import_source 'products', 'filter[active]=1&filter[sort]=[id_ASC]'
      products = get_request

      ids = []
      xml = Nokogiri::XML::Document.parse(products)
      xml.xpath("//prestashop//products//product").each do |p|
        ids << p.attributes['id'].content.to_i
      end
      ids.sort!
    end

    def all
      ids = get_ids
      return 'No products' if ids.blank?

      result = []
      imported = 0
      ids.each do |id|
        if r = item(id).nil?
          imported += 1
        else
          result << r if r.kind_of?(String)
        end

      end

      "Imported: #{imported}, Bad products: #{result.join(',')}"
    end

    def item id
      return unless id

      # Get product by id
      set_import_source "products/%s" % id
      product = get_request
      return id if get_request.nil?

      xml = Nokogiri::XML::Document.parse(product)
      @product_xml = xml
      data = build_data
      return 'disabled' if data.blank?

      xml.xpath("//prestashop//product//combinations//combinations//id").each do |combination|
        cid = combination.content
        data[:uliss_combination_id] = cid
        quantity = item_quantity cid

        # Find product by uliss__product_id
        product = Product.where(uliss_product_id: cid).first
        if product
          #item_update(product, {quantity: quantity})
          product.update!({quantity: quantity, uliss_product_id: cid})
          product.product_group.update!({uliss_product_id: id})
          puts "updated: cid=#{cid}, id=#{id}, qty=#{quantity}"
        else
          data[:quantity] = quantity

          uliss_product = UlissProduct.where(uliss_id: id, uliss_combination_id: cid).first
          unless uliss_product
            item_create(cid, data)

            # Product images
            images = xml.xpath("//prestashop//product//associations//images//image//id")
            unless images.blank?
              images.each do |image|
                item_images id, image.content.to_i
              end
            end
          end

        end

      end
    end

    def item_update product, data
      return unless product

      product.update!(data)
    end

    def item_create cid, data
      return unless cid

      lang = set_language

      # Get combination by id
      set_import_source "combinations/%s" % cid
      combination = get_request
      xml = Nokogiri::XML::Document.parse(combination)

      # Get quantity

      uliss_product = UlissProduct.create!(data)
      if uliss_product
        uliss_product_id = uliss_product.id
        Rails.logger.info('UlissProduct created [ID=%s]' % uliss_product_id)
      else
        Rails.logger.info(uliss_product.errors.full_messages.join('|'))
        return
      end

      product_option_values_ids = []
      xml.xpath("//prestashop//combination//associations//product_option_values//product_option_value//id").each do |pov|
        product_option_values_ids << pov.content.to_i
      end

      product_option_values_ids.each do |pov_id|
        # Get product_option_value by id
        set_import_source "product_option_values/%s" % pov_id
        product_option_value = get_request
        xml = Nokogiri::XML::Document.parse(product_option_value)

        pov_name = xml.xpath("//prestashop//product_option_value//name//language[@id=#{lang}]").last.content
        product_option_id = xml.xpath("//prestashop//product_option_value//id_attribute_group").first.content

        # Get product_option by id
        set_import_source "product_options/%s" % product_option_id
        product_option = get_request
        xml = Nokogiri::XML::Document.parse(product_option)
        product_option_name = xml.xpath("//prestashop//product_option//name//language[@id=#{lang}]").last.content


        # product option
        emporio_product_option = ProductOption.find_by_name(product_option_name)
        if emporio_product_option.blank?
          emporio_product_option = ProductOption.new({name: product_option_name})
          if emporio_product_option.save
            Rails.logger.info('Product option created [ID=%s]' % emporio_product_option.id)
          else
            Rails.logger.info(emporio_product_option.errors.full_messages.join('|'))
            return
          end
        end

        # product option value
        emporio_product_option_value = ProductOptionValue.find_by_name(pov_name)
        if emporio_product_option_value.blank?
          emporio_product_option_value = ProductOptionValue.new({product_option_id: emporio_product_option.id, name: pov_name})
          if emporio_product_option_value.save
            Rails.logger.info('Product option value created [ID=%s]' % emporio_product_option_value.id)
          else
            Rails.logger.info(emporio_product_option_value.errors.full_messages.join('|'))
            return
          end
        end


        uliss_product_combination = UlissProductCombination.create!({uliss_product_id: uliss_product_id,
                                                                     product_option_id: emporio_product_option.id,
                                                                     product_option_value_id: emporio_product_option_value.id})
      end

    end

    def item_quantity cid
      return 0 unless cid

      quantity = 0
      set_import_source 'stock_availables', 'filter[id_product_attribute]=%s' % cid
      stock = get_request
      xml = Nokogiri::XML::Document.parse(stock)
      stock_id = xml.xpath("//prestashop//stock_availables//stock_available").map{|s| s.attributes['id'].content.to_i}.first
      if stock_id
        set_import_source 'stock_availables/%s' % stock_id
        stock = get_request
        xml = Nokogiri::XML::Document.parse(stock)

        combination_id = xml.xpath("//prestashop//stock_available//id_product_attribute").first.content
        if combination_id == cid
          quantity = xml.xpath("//prestashop//stock_available//quantity").first.content
        end
      end
      quantity
    end

    def import_product_manufacturer name
      return nil unless name
      return if name.empty?

      manufacturer_id = nil
      manufacturer = Manufacturer.where(name: name)
      if manufacturer.blank?
        manufacturer_id = Manufacturer.create!(name: name).id
      else
        manufacturer_id = manufacturer.first.id
      end
      manufacturer_id
    end

    def import_product_supplier name
      return nil unless name

      supplier_id = nil
      supplier = Supplier.where("name LIKE '%#{name}%' ")
      if supplier.blank?
        supplier_id = Supplier.create!(name: name).id
      else
        supplier_id = supplier.first.id
      end
      supplier_id
    end

    def item_images id, mid
      return unless id && mid

      tmp = Tempfile.new('uliss-product-image', Dir.tmpdir, 'wb+')
      filename = tmp.path.gsub('/','-')[1..-1]

      set_import_source 'images/products/%s/%s' % [id,mid]
      begin
        request = open(@url)
      rescue => error
        Rails.logger.info('Error: %s' % error)
      end

      if request
        IO.copy_stream(request, tmp)
        tmp.rewind
        new_file = "#{Rails.root}/public/uploads/tmp/#{filename}.jpg"
        FileUtils.mv(tmp.path, new_file)
        image = Image.new({
                              image: File.open(new_file),
                              origin_id: mid
                          })

        if image.save
          UlissProductImage.create!({
                                        uliss_origin_id: id,
                                        image_id: image.id
                                    })
          File.unlink(new_file)
        end

      end

    end



    private

    def set_language
      2
    end

    def build_data
      lang = set_language

      data = {}
      data[:name] = @product_xml.xpath("//prestashop//product//name//language[@id=#{lang}]").last.content
      data[:description] = @product_xml.xpath("//prestashop//product//description//language[@id=#{lang}]").last.content
      active = @product_xml.xpath("//prestashop//product//active").first.content.to_i
      return {} if active == 0

      data[:active_state] = active == 1 ? 'enable' : 'disable'
      data[:uliss_id] = @product_xml.xpath("//prestashop//product//id").first.content
      data[:list_price] = @product_xml.xpath("//prestashop//product//wholesale_price").first.content
      data[:manufacturer_number] = @product_xml.xpath("//prestashop//product//reference").first.content
      data[:state] = 'new'

      # Supplier
      data[:supplier_id] = import_product_supplier @name

      # Manufacturer
      #manufacturer_name = @product_xml.xpath("//prestashop//product//manufacturer_name").first.content
      #data[:manufacturer_id] = import_product_manufacturer manufacturer_name

      # Get stock availables
      #stock_availables = @product_xml.xpath("//prestashop//product//associations//stock_availables//stock_available//id")
      data
    end

    def set_import_source v, params = nil
      @source = v
      import_url params
    end

    def populate_relation
      supplier_id = import_product_supplier @name
      products = Product.where(supplier_id: supplier_id)

      products.each do |product|
        product_group = product.product_group
        uliss_product_id = product_group.uliss_product_id
        unless uliss_product_id.nil?
          product_option_values = product.product_option_combinations.map{|i| [i.product_option.name,i.product_option_value.name]}

          uliss_product_option_values = []
          product_option_values.each do |option|
            set_import_source 'product_options', 'filter[name]=[%s]' % option[0]
            uliss_product_option = get_request
            xml = Nokogiri::XML::Document.parse(uliss_product_option)
            uliss_product_option_id = xml.xpath("//prestashop//product_options/product_option").map{|i| i.attributes['id'].content}.first

            set_import_source 'product_option_values', 'filter[id_attribute_group]=%s&filter[name]=[%s]' % [uliss_product_option_id,option[1].strip]
            uliss_product_option_value = get_request
            xml = Nokogiri::XML::Document.parse(uliss_product_option_value)
            uliss_product_option_value_id = xml.xpath("//prestashop//product_option_values/product_option_value").map{|i| i.attributes['id'].content}.first

            uliss_product_option_values << uliss_product_option_value_id if uliss_product_option_value_id
          end

          set_import_source 'products/%s' % uliss_product_id
          uliss_product = get_request
          return nil if uliss_product.nil?

          xml = Nokogiri::XML::Document.parse(uliss_product)

          if uliss_product_id
            set_import_source 'combinations', 'filter[id_product]=%s' % uliss_product_id
            uliss_combinations = get_request
            xml = Nokogiri::XML::Document.parse(uliss_combinations)

            uliss_combination_ids = xml.xpath("//prestashop//combinations//combination").map{|i| i.attributes['id'].content}
          end

          @uliss_combination_id = 0
          uliss_combination_ids.each do |combination|
            set_import_source 'combinations/%s' % combination
            uliss_combination = get_request
            xml = Nokogiri::XML::Document.parse(uliss_combination)
            pov = xml.xpath("//prestashop//combination//associations//product_option_values//product_option_value//id").map{|i| i.content}

            if (uliss_product_option_values - pov).empty?
              @uliss_combination_id = combination.to_i
            end

          end

          product.update!({uliss_product_id: @uliss_combination_id}) if @uliss_combination_id > 0
        end

      end
    end


  end



end

