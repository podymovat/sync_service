class Sync::ManufacturersService

  def initialize options = {}
    @options = options
  end

  # Single export method
  def export
    ws = Export.new
    ws.all
  end

  # Single import method
  def import
    ws = Import.new
    ws.all
  end


  # Import class
  class Import < Sync::ImportBase

    def initialize options = {}
      @resource = 'manufacturers'
    end

    def all
      get_karimini_ids.map do |id|
        manufacturer = Manufacturer.where(origin_id: id).first
        action = set_action manufacturer
        self.send(action, id)
        item_image id if action == 'item_update'
      end
    end

    def item_create id
      return unless id

      set_import_source "#{@resource}/%s" %id
      @result = json_parse get_request, @resource.singularize
      data = build_data
      data[:origin_id] = id

      manufacturer = Manufacturer.new(data)
      manufacturer = create_object manufacturer
      return nil if manufacturer.nil?
      manufacturer
    end


    def item_update id
      manufacturer = Manufacturer.where(origin_id: id).first
      return if manufacturer.blank?
      return unless in_sync? manufacturer

      set_import_source "#{@resource}/%s" % id
      @result = json_parse get_request, @resource.singularize
      data = build_data
      data[:origin_id] = id

      manufacturer = update_object manufacturer, data
      return nil if manufacturer.nil?
      manufacturer.id
    end

    def item_image id
      return unless id

      tmp = Tempfile.new('karimini-manufacturer-image', Dir.tmpdir, 'wb+')
      filename = tmp.path.gsub('/','-')[1..-1]

      set_import_source "images/#{@resource}/%s" % id
      begin
        request = open(@url)
      rescue => error
        Rails.logger.info('Error: %s' % error)
      end

      if request
        IO.copy_stream(request, tmp)
        tmp.rewind
        new_file = "#{Rails.root}/public/uploads/tmp/#{filename}.jpg"
        FileUtils.mv(tmp.path, new_file)
        data = {
            image: File.open(new_file),
        }

        manufacturer = Manufacturer.where(origin_id: id).first
        if manufacturer.update(data)
          File.unlink(new_file)
        else
          Rails.logger.info(manufacturer.errors.full_messages.join('|'))
        end

      end
    end


    private

    def build_data
      {
        name: @result['name'],
        description: @result['description'][0]['value'],
        short_description: @result['short_description'][0]['value'],
        meta_title: @result['meta_title'][0]['value'],
        meta_keywords: @result['meta_keywords'][0]['value'],
        meta_description: @result['meta_description'][0]['value']
      }
    end



  end


  # Export class
  class Export < Sync::ExportBase

      def initialize options = {}
        @resource = 'manufacturers'
      end


      def all
        manufaturers = Manufacturer.all.order('origin_id asc')
        manufaturers.each do |manufacturer|
          item manufacturer
        end
      end

      def item manufacturer
        return unless manufacturer

        @object = manufacturer
        data = build_data

        kid = manufacturer.origin_id
        action = set_action manufacturer
        data[:id] = kid if action == 'update'

        set_export_source @resource, action
        response = post_request data
        result = json_parse response

        if result['status'] == 'success'
          manufacturer_id = result['manufacturer_id']['0'].to_i
          manufacturer.update!({origin_id: manufacturer_id}) if manufacturer_id > 0
        end

        result
      end


      private

      def build_data
        {
          name: @object.name,
          description: @object.description,
          meta_title: @object.meta_title,
          meta_keywords: @object.meta_keywords,
          meta_description: @object.meta_description
        }
      end


  end







end
