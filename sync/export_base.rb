class Sync::ExportBase
  include Sync::ApiBase

  attr_accessor :object,
                :export_data

  require 'nokogiri'

  def dates_filter from = nil, to = nil
    now = Time.now.utc.strftime('%Y-%m-%d')
    tomorrow = (Time.now + 1.days).utc.strftime('%Y-%m-%d')
    range = nil

    from = from.to_datetime if from && from.kind_of?(String)
    from = from.utc.strftime('%Y-%m-%d') if from && from.is_a?(Date)

    to = to.to_datetime if to && to.kind_of?(String)
    to = to.utc.strftime('%Y-%m-%d') if to && to.is_a?(Date)

    range = Range.new(now,tomorrow) if from.nil? && to.nil?
    range = Range.new(to,tomorrow) if to && from.nil?
    range = Range.new(from,now) if from && to.nil? && from < now
    range = Range.new(now,tomorrow) if from && to.nil? && from == now
    range = nil if from && to.nil? && from > now
    range = Range.new(from,to) if from && to && from < to
    range = Range.new(to,from) if from && to && to < from

    range.is_a?(Range) ? {updated_at: range} : "updated_at LIKE '#{range}%' "
  end

  def set_action obj
    kid = obj.origin_id
    if in_sync? obj
      set_import_source "#{@resource}/%s" % kid
      karimini_obj = get_request
      if karimini_obj.nil?
        action = 'create'
      else
        action = 'update'
      end
    else
      action = 'create'
    end
    action
  end

  def json_parse json
    return {} unless json

    begin
      JSON.parse json
    rescue
      nil
    end

  end

end
