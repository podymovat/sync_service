# Entities
#   product_groups
#   categories
#   manufacturers
#   suppliers
#
# Sync.import entity, {type, date_from, date_to, attrs, qty}
# type: 'all', 'by_date'
# 'all' - by default


module Sync
  class << self
    attr_accessor :entity_class

    def import entity, options = {}
      set_entity_class entity
      service = @entity_class.new options
      service.import
    end

    def export entity, options = {}
      set_entity_class entity
      service = @entity_class.new options
      service.export
    end

    def test strategy
      return if strategy.empty? || strategy.nil?
      Sync::ApiBase.send("#{strategy}_test")
    end


    private

    def set_entity_class e
      raise Sync::Error.new(e) if e.empty?
      @entity_class = entity_to_class e
    end

    def entity_to_class e
      class_name = "Sync::#{e.capitalize.camelize}Service".safe_constantize
      raise Sync::Error.new(e) if class_name.nil?
      class_name
    end

  end


  class Error < StandardError
    attr_reader :object
    def initialize(object)
      @object = object
    end
    def message
      "Wrong entity name - '#{@object}'"
    end
  end


end
